import math

import numpy as np
import cv2.cv2 as cv2
import matplotlib.pyplot as plt

def compute_hamma(theta, beta, alpha):


    if 0 <= theta < 180:
        hamma = beta + (90 + theta)

    elif 180 <= theta <= 270:

        if theta-180 < alpha:
            hamma = beta + (90 + theta)

        elif theta-180 == alpha:
            hamma = 0

        elif theta-180 > alpha:
            hamma = (beta - ((360 - theta)-90))

    elif 270 < theta <= 360:
        hamma = beta + (90 - (360 - theta)) 

    if hamma > 360: 
        raise ValueError('Param hamma > 360')

    return hamma

def n_max(accumulator):


    per = 0.85

    (i, j, n, m) = np.unravel_index(accumulator.argmax(), accumulator.shape)

    (q, w, e, r) = accumulator.shape



    print('Shape: %s %s %s %s ' % (q, w, e, r))
    print('Max: %s -- Min: %s' % (max,min))

def analize_accumalator(accumulator):

    blured = cv2.medianBlur(accumulator, 5)
    ret, threshold_0 = cv2.threshold(blured, 220, 255, cv2.THRESH_BINARY)
    #threshold = cv2.adaptiveThreshold(accumulator, 10, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    
    dist = cv2.distanceTransform(threshold_0, cv2.DIST_L2, 5)

    normal = cv2.normalize(dist, 0.0, 1.0, cv2.NORM_MINMAX)

    ret, threshold_1 = cv2.threshold(normal, 0.02, 1, cv2.THRESH_BINARY)

    plt.clf()
    plt.gray()

    plt.imshow(threshold_1)
    plt.show()
