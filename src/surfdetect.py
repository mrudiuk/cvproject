import os

import cv2.cv2 as cv2
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

from settings import * 


def standart_surf_detect(kps1, descs1, kps2, descs2, img1, img2):

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(descs1, descs2, k=2)

    good = []

    for m, n in matches:
        if m.distance < DISTANCE_CORRECT*n.distance:
            good.append(m)

    print(len(good))

    if len(good) > MATCH_THRESHOLD:

        src_pts = np.float32([kps1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2) 
        dst_pts = np.float32([kps2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        matchesMask = mask.ravel().tolist()

        (h, w) = img1.shape[:-1]
        pts = np.float32([ [0, 0], [0, h-1], [w-1, h-1], [w-1, 0] ]).reshape(-1, 1, 2)
        dst = cv2.perspectiveTransform(pts, M)

        img2 = cv2.polylines(img2, [np.int32(dst)], True, (255, 0, 0), 3, cv2.LINE_AA)

    else:
        print("Not enough matches are found - %d / %d" % (len(good),MATCH_THRESHOLD))
        matchesMask = None
    

    draw_params = dict(matchColor = (0, 255, 0),
                       singlePointColor = None,
                       #matchesMask = matchesMask, 
                       flags=2)


    img3 = cv2.drawMatches(img1, kps1, img2, kps2, good, None, **draw_params)

    plt.figure('SURF')
    plt.imshow(img3)
    plt.show()
