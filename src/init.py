# Standart Library Modules
import os
import time
from collections import defaultdict

# Download Modules
# import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import cv2.cv2 as cv2

# Local Modules
from settings import *
from features import Features
import generallizedhough as ght
# import segmendetect as sgm
# import surfdetect as sfdt


def init():

    print('SEARCH OBJECTS')
    start = time.time()

    names = ['plombir', 'cream_plombir']
    assortment = defaultdict(list)

    for name in names:

        QUERY = os.path.join('../images', name + '.jpg')
        origin_query = mpimg.imread(QUERY)
        query = origin_query.copy()
        query = cv2.cvtColor(origin_query, cv2.COLOR_BGR2GRAY)

        h, w = query.shape[:2]

        y = int(h // 2)
        x = int(w // 2)
        reference_point = (x, y)

        TRAIN = os.path.join('../images', '3.jpeg')
        origin_train = mpimg.imread(TRAIN)
        train = origin_train.copy()
        train = cv2.cvtColor(origin_train, cv2.COLOR_BGR2GRAY)

        # Get matches and keypoints
        features = Features(query, train)
        query_kps, train_kps = features.get_kps()
        query_descs, train_descs = features.get_descs()
        matches = features.get_matches()

        if len(matches) < MATCH_THRESHOLD:
            print("NO MATCHES")
            assortment[name] = 'not found'

            # img = cv2.cvtColor(train, cv2.COLOR_GRAY2BGR)
            plt.figure('NO MATCHES')
            plt.title(name)
            plt.imshow(train)
            plt.show()

        else:

            # Generallized Hough Transform
            r_table = ght.build_r_table(query, query_kps,
                                        matches, reference_point)
            accumulator = ght.accumulate_kps(r_table, train,
                                             train_kps, matches)
            assortment[name] = 'found'
            elapsed = time.time() - start
            print('Time: ', elapsed, ' seconds')

            ght.draw_segment(accumulator, train, name)

    f = open('assortment.txt', 'w')
    for key in assortment:
        f.write(key + '---' + assortment[key] + '\n')
        print('%s : %s' % (key, assortment[key]))

    f.close()


if __name__ == '__main__':
    init()
