# Download Modules
import cv2.cv2 as cv2
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Local Modules
from settings import * 


def create_classic_accumulator(train_kps, matches, train):

    (height, width) = train.shape[:2]
    train_kps_pt = np.float64([train_kps[m.trainIdx].pt for m in matches])

    width_array = int(width/BIN_SIZE)+CORRECT_SHAPE
    height_array = int(height/BIN_SIZE)+CORRECT_SHAPE
    
    accumulator = np.zeros((height_array, width_array), dtype=np.int)

    for kp_pt in train_kps_pt:

        x, y = kp_pt

        idx_x = int(x/BIN_SIZE)
        idx_y = int(y/BIN_SIZE)

        accumulator[idx_y][idx_x] += 1
                
    return accumulator


def draw_accumulator(accumulator):

    h, w = accumulator.shape

    _x = np.arange(0, w, 1)
    _y = np.arange(0, h, 1)

    _xx, _yy = np.meshgrid(_x, _y)

    x, y = _xx.ravel(), _yy.ravel()
    top = accumulator.ravel()
    bottom = np.zeros_like(top)
    width = depth = 1

    figure = plt.figure()
    axes = figure.add_subplot(111, projection='3d')
    axes.bar3d(x, y, bottom, width, depth, top, shade=True)
    
    plt.show()


def get_max_zones(accumulator):

    max_range = accumulator.max()
    min_range = max_range * ROI_SIZE

    rows, cols = accumulator.shape
    zones_data = []
    zones = []

    for row in range(0, rows):
        for col in range(0, cols):

            if accumulator[row][col] >= min_range:
                zones_data.append((row-1, row+2, col-1, col+2))

    return zones_data


def get_segment(ROI, train):

    segments = []
    coordinats = []

    for sgm in ROI:

        y1, y2, x1, x2 = sgm
          
        start_row = y1 * BIN_SIZE
        start_col = x1 * BIN_SIZE

        end_row = y2 * BIN_SIZE
        end_col = x2 * BIN_SIZE

        coordinats.append((start_row, end_row, start_col, end_col))

        ROI = train[start_row:end_row, start_col:end_col]

        #plt.imshow(ROI)
        #plt.show()

        segments.append(ROI)

    return segments, coordinats


def find_zones_matches(coordinats, train_kps, matches):
    
    zones_matches = {}

    for zone in coordinats:
        y1, y2, x1, x2 = zone
        for m in matches:
            x, y = train_kps[m.trainIdx].pt
            if x1 < x < x2 and y1 < y < y2:
                zones_matches.setdefault(zone, []).append(m)

    return zones_matches


def draw(query, train, query_kps, train_kps, matches):

    for item in matches.items():

        zona, matches = item

        src_pts = np.float32([query_kps[m.queryIdx].pt for m in matches]).reshape(-1, 1, 2) 
        dst_pts = np.float32([train_kps[m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)


        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5)
        matchesMask = mask.ravel().tolist()

        (h, w) = query.shape[:-1]
        pts = np.float32([ [0, 0], [0, h-1], [w-1, h-1], [w-1, 0] ]).reshape(-1, 1, 2)
        dst = cv2.perspectiveTransform(pts, M)

        train = cv2.polylines(train, [np.int32(dst)], True, (255, 0, 0), 2, cv2.LINE_AA)


        draw_params = dict(matchColor = None,
                           singlePointColor = None,
                           matchesMask = matchesMask, 
                           flags=2)

        img3 = cv2.drawMatches(query, query_kps, train, train_kps, matches, None, **draw_params)
        

    plt.figure('Detected objects')
    plt.title('Objects')
    plt.imshow(train)
    plt.show()